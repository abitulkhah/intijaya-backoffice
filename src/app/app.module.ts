import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LayoutComponent } from './layout/layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BarangComponent } from './pages/barang/barang.component';
import { CustomerComponent } from './pages/customer/customer.component';
import { KurirComponent } from './pages/kurir/kurir.component';
import { KendaraanComponent } from './pages/kendaraan/kendaraan.component';
import { PemesananBarangComponent } from './pages/pemesanan-barang/pemesanan-barang.component';
import { SuratJalanComponent } from './pages/surat-jalan/surat-jalan.component';
import { InvoiceComponent } from './pages/invoice/invoice.component';
import { KwitansiComponent } from './pages/kwitansi/kwitansi.component';
import { ReturBarangComponent } from './pages/retur-barang/retur-barang.component';
import { LaporanPembayaranComponent } from './pages/laporan-pembayaran/laporan-pembayaran.component';
import { LaporanPenjualanComponent } from './pages/laporan-penjualan/laporan-penjualan.component';
import { LaporanPengirimanBarangComponent } from './pages/laporan-pengiriman-barang/laporan-pengiriman-barang.component';
import { LaporanReturBarangComponent } from './pages/laporan-retur-barang/laporan-retur-barang.component';
import { ModelBarangComponent } from './pages/model-barang/model-barang.component';
import { UserComponent } from './pages/user/user.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { BuktiTerimaComponent } from './pages/bukti-terima/bukti-terima.component';
import { ListPemesananComponent } from './pages/list-pemesanan/list-pemesanan.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    BarangComponent,
    CustomerComponent,
    KurirComponent,
    KendaraanComponent,
    PemesananBarangComponent,
    SuratJalanComponent,
    InvoiceComponent,
    KwitansiComponent,
    ReturBarangComponent,
    LaporanPembayaranComponent,
    LaporanPenjualanComponent,
    LaporanPengirimanBarangComponent,
    LaporanReturBarangComponent,
    ModelBarangComponent,
    UserComponent,
    LoginComponent,
    HomeComponent,
    BuktiTerimaComponent,
    ListPemesananComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
