import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { CustomerComponent } from './pages/customer/customer.component';
import { BarangComponent } from './pages/barang/barang.component';
import { InvoiceComponent } from './pages/invoice/invoice.component';
import { SuratJalanComponent } from './pages/surat-jalan/surat-jalan.component';
import { PemesananBarangComponent } from './pages/pemesanan-barang/pemesanan-barang.component';
import { LaporanReturBarangComponent } from './pages/laporan-retur-barang/laporan-retur-barang.component';
import { LaporanPenjualanComponent } from './pages/laporan-penjualan/laporan-penjualan.component';
import { LaporanPengirimanBarangComponent } from './pages/laporan-pengiriman-barang/laporan-pengiriman-barang.component';
import { LaporanPembayaranComponent } from './pages/laporan-pembayaran/laporan-pembayaran.component';
import { KwitansiComponent } from './pages/kwitansi/kwitansi.component';
import { KurirComponent } from './pages/kurir/kurir.component';
import { KendaraanComponent } from './pages/kendaraan/kendaraan.component';
import { ModelBarangComponent } from './pages/model-barang/model-barang.component';
import { UserComponent } from './pages/user/user.component';
import { LoginComponent } from './pages/login/login.component';
import { ReturBarangComponent } from './pages/retur-barang/retur-barang.component';
import { HomeComponent } from './pages/home/home.component';
import { BuktiTerimaComponent } from './pages/bukti-terima/bukti-terima.component';
import { ListPemesananComponent } from './pages/list-pemesanan/list-pemesanan.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full'
  },
  {
    path: 'login',
    component : LoginComponent
  },
  {
    path: 'menu',
    component: LayoutComponent,
    children: [
      {
        path: '', redirectTo: 'home', pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'customer',
        component: CustomerComponent
      },
      {
        path: 'barang',
        component: BarangComponent
      },
      {
        path: 'model-barang',
        component: ModelBarangComponent
      },
      {
        path: 'invoice',
        component: InvoiceComponent
      },
      {
        path: 'kendaraan',
        component: KendaraanComponent
      },
      {
        path: 'kurir',
        component: KurirComponent
      },
      {
        path: 'kwitansi',
        component: KwitansiComponent
      },
      {
        path: 'laporan-pembayaran',
        component: LaporanPembayaranComponent
      },
      {
        path: 'laporan-pengiriman-barang',
        component: LaporanPengirimanBarangComponent
      },
      {
        path: 'laporan-penjualan',
        component: LaporanPenjualanComponent
      },
      {
        path: 'laporan-retur-barang',
        component: LaporanReturBarangComponent
      },
      {
        path: 'pemesanan-barang',
        component: PemesananBarangComponent
      },
      {
        path: 'retur-barang',
        component: ReturBarangComponent
      },
      {
        path: 'surat-jalan',
        component: SuratJalanComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'bukti-terima',
        component: BuktiTerimaComponent
      },
      {
        path: 'list-pemesanan',
        component: ListPemesananComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
