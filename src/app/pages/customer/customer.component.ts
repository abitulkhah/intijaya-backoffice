import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { CustomerService } from '../../providers/menu/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  kodeData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private customerService: CustomerService
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.customerService.getCustomer().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
        console.log(this.tableData);
      }
    });
    this.isShowForm = false;
  }

  generateForm(){
    this.formData = this.fb.group({
      namaCustomer : [ null, [ Validators.required ] ],
      alamat : [ null, [ Validators.required ] ],
      noHp : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    this.isShowForm = true;
    if(data){
      this.kodeData = data.noCustomer;
      this.formData.patchValue(data);
    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    //edit data
    if(this.kodeData){
      data.noCustomer = this.kodeData;
      this.customerService.updateCustomer(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
    //add data 
    else{
      this.customerService.postCustomer(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
  }

  deleteData(noCustomer){
    console.log("delete");
    this.customerService.deleteCustomer({noCustomer: noCustomer}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    this.isShowForm = false;
    if(this.kodeData){
      this.kodeData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


}
