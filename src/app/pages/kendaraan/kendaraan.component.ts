import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { KendaraanService } from '../../providers/menu/kendaraan.service';

@Component({
  selector: 'app-kendaraan',
  templateUrl: './kendaraan.component.html',
  styleUrls: ['./kendaraan.component.scss']
})
export class KendaraanComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  kodeData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private kendaraanService: KendaraanService
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.kendaraanService.getKendaraan().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
    this.isShowForm = false;
  }

  generateForm(){
    this.formData = this.fb.group({
      typeKendaraan : [ null, [ Validators.required ] ],
      namaKendaraan : [ null, [ Validators.required ] ],
      nopol: [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    this.isShowForm = true;
    if(data){
      this.kodeData = data.kodeKendaraan;
      this.formData.patchValue(data);
    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    delete data.imageKadoStory;

    //edit data
    if(this.kodeData){
      data.kodeKendaraan = this.kodeData;
      this.kendaraanService.updateKendaraan(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
    //add data 
    else{
      this.kendaraanService.postKendaraan(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
  }

  deleteData(kodeKendaraan){
    this.kendaraanService.deleteKendaraan({kodeKendaraan: kodeKendaraan}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    this.isShowForm = false;
    if(this.kodeData){
      this.kodeData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


}
