import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuktiTerimaComponent } from './bukti-terima.component';

describe('BuktiTerimaComponent', () => {
  let component: BuktiTerimaComponent;
  let fixture: ComponentFixture<BuktiTerimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuktiTerimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuktiTerimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
