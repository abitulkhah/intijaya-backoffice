import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { BuktiTerimaService } from '../../providers/menu/bukti-terima.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-bukti-terima',
  templateUrl: './bukti-terima.component.html',
  styleUrls: ['./bukti-terima.component.scss']
})
export class BuktiTerimaComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private buktiTerimaService: BuktiTerimaService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.buktiTerimaService.getBuktiTerima().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
  }

  generateForm(){
    this.formData = this.fb.group({
      tglKwitansi : [ null, [ Validators.required ] ],
      jmlBayar : [ null, [ Validators.required ] ],
      noInvoice : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    console.log("show form");
    this.isShowForm = true;
    if(data){

    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    this.buktiTerimaService.postBuktiTerima(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
    });
  }

  deleteData(id){
    console.log("delete");
    this.buktiTerimaService.deleteBuktiTerima({id: id}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    if(this.idData){
      this.idData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


  printData(data:any){
    const doc = new jsPDF();

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Bukti Terima", 90, 50);

    doc.autoTable({
      head: [['Tgl Bukti Terima', 'No Bukti Terima', 'No Surat Jalan']],
      body: [[moment(data.tglReceipt).format("DD-MM-YYYY"), data.noReceipt, data.suratJalan.noSuratJalan]],
      margin: {top: 70}
    });

    doc.save('Bukti Terima.pdf');
  }

}
