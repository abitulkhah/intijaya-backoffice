import { Component, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { PemesananBarangService } from '../../providers/menu/pemesanan-barang.service';
import { BarangService } from '../../providers/menu/barang.service';
import { CustomerService } from '../../providers/menu/customer.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-pemesanan-barang',
  templateUrl: './pemesanan-barang.component.html',
  styleUrls: ['./pemesanan-barang.component.scss']
})
export class PemesananBarangComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;
  dp = null;
  grandTotal = 0;
  sisaBayar = null;
  tglPemesanan = null;
  listCustomer = [];
  listBarang = [];
  noCustomer = null;
  namaCustomer = null;
  invalidData =  true;
  editedKodeBarang = null;
  disbaleKodeBarang = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private pemesananBarangService: PemesananBarangService,
    private barangService: BarangService,
    private customerService: CustomerService
  ){}

  ngOnInit(): void {
    this.generateForm();
    console.log(this.tableData);
    
    this.barangService.getBarang().subscribe(response=>{
        this.listBarang = response.data;
    });

    this.customerService.getCustomer().subscribe(response=>{
        this.listCustomer = response.data;
    });

  }

  generateForm(){
    this.formData = this.fb.group({
      kodeBarang : [ null, [ Validators.required ] ],
      jmlPemesanan : [ null, [ Validators.required ] ],
      ketBarang :[ null, [ Validators.required ] ],
      namaBarang: [null],
      harga: [null]
    });
  }

  showForm(data){
    console.log("show form");
    
    if(data){
      this.formData.patchValue(data);
      this.editedKodeBarang = data.kodeBarang;
      this.formData.get('kodeBarang').disable();
    }
  }

  selectBarang(kodeBarang: any){
    let index = this.listBarang.map(object=>{return object.kodeBarang;}).indexOf(kodeBarang);
    console.log(index);
    if(kodeBarang){
      this.formData.controls["namaBarang"].patchValue(this.listBarang[index].namaBarang);
      this.formData.controls["harga"].patchValue(this.listBarang[index].harga);
    }
  }

  selectCustomer(noCustomer: any){
    let index = this.listCustomer.map(object=>{return object.noCustomer;}).indexOf(noCustomer);
    console.log(index);
    this.namaCustomer = this.listCustomer[index].namaCustomer;
    if(this.sisaBayar && this.tglPemesanan){
      this.invalidData = false;
    }
  }

  selectDate(){
    if(this.namaCustomer && this.sisaBayar){
      this.invalidData = false;
    } 
  }

  changeDP(){
 
    this.sisaBayar = this.grandTotal - this.dp;

    if(this.namaCustomer && this.tglPemesanan){
      this.invalidData = false;
    }
  }

  addData(e){

    let newData = this.formData.value;
    if (this.editedKodeBarang) {
      let index = this.tableData.map(object=>{return object.kodeBarang;}).indexOf(this.editedKodeBarang);
      this.tableData[index].jmlPemesanan = newData.jmlPemesanan;
      this.tableData[index].ketBarang = newData.ketBarang;
    } else {
      newData.hargaTotal = newData.harga * newData.jmlPemesanan;
      this.tableData.push(newData);
    }
  
    this.tableData = this.tableData.map(object=>{return object;});

    for (let i = 0; i < this.tableData.length; i++) {
      this.grandTotal = this.grandTotal + this.tableData[i].hargaTotal;
    }

    console.log(this.grandTotal);

    console.log(this.tableData);
    this.resetForm(e);
  }

  submitForm(e){

    this.loadingSubmit = true;
    let data = {
      dp : this.dp,
      grandTotal: this.grandTotal,
      noCustomer: this.noCustomer,
      sisaBayar: this.sisaBayar,
      tglPemesanan: this.tglPemesanan,
      detailPemesananDtoList: this.tableData
    };

    this.pemesananBarangService.postpemesananBarang(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetCustomer(e);
          this.resetForm(e);
          this.ngOnInit();
        }
    });
  }

  deleteData(kodeBarang){
    let index = this.tableData.map(object=>{return object.kodeBarang;}).indexOf(kodeBarang);
    this.tableData.splice(index,1);
    this.tableData = this.tableData.map(object=> {return object;});
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    

    if(this.idData){
      this.idData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }
  
  resetCustomer(e){
    this.tableData = [];
    this.dp = 0;
    this.grandTotal = 0;
    this.sisaBayar = null;
    this.tglPemesanan = null;
    this.noCustomer = null;
    this.namaCustomer = null;
  }

}
