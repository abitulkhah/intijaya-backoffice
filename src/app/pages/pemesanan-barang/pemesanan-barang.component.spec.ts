import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PemesananBarangComponent } from './pemesanan-barang.component';

describe('PemesananBarangComponent', () => {
  let component: PemesananBarangComponent;
  let fixture: ComponentFixture<PemesananBarangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemesananBarangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PemesananBarangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
