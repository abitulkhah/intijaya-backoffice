import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { KurirService } from '../../providers/menu/kurir.service';

@Component({
  selector: 'app-kurir',
  templateUrl: './kurir.component.html',
  styleUrls: ['./kurir.component.scss']
})
export class KurirComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  kodeData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private kurirService: KurirService
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.kurirService.getKurir().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
    this.isShowForm = false;
  }

  generateForm(){
    this.formData = this.fb.group({
      namaKurir : [ null, [ Validators.required ] ],
      alamatKurir : [ null, [ Validators.required ] ],
      noHp : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    this.isShowForm = true;
    if(data){
      this.kodeData = data.kodeKurir;
      this.formData.patchValue(data);
    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    delete data.imageKadoStory;

    //edit data
    if(this.kodeData){
      data.kodeKurir = this.kodeData;
      this.kurirService.updateKurir(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
    //add data 
    else{
      console.log(JSON.stringify(data));
      this.kurirService.postKurir(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
  }

  deleteData(kodeKurir){
    console.log("delete");
    this.kurirService.deleteKurir({kodeKurir: kodeKurir}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    this.isShowForm = false;
    if(this.kodeData){
      this.kodeData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


}
