import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { SuratJalanService } from '../../providers/menu/surat-jalan.service';
import { InvoiceService } from '../../providers/menu/invoice.service';
import { KurirService } from '../../providers/menu/kurir.service';
import { KendaraanService } from '../../providers/menu/kendaraan.service';
import { ImageEncoder } from 'src/app/utils/image-encoder';
import * as moment from 'moment';
declare let jsPDF:any;

@Component({
  selector: 'app-surat-jalan',
  templateUrl: './surat-jalan.component.html',
  styleUrls: ['./surat-jalan.component.scss']
})
export class SuratJalanComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  invoiceList = [];
  kurirList = [];
  kendaraanList = [];
  formData: FormGroup;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private suratJalanService: SuratJalanService,
    private invoiceService: InvoiceService,
    private kurirService: KurirService,
    private kendaraanService: KendaraanService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.suratJalanService.getSuratJalan().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });

    this.invoiceService.getInvoice().subscribe(response=>{
        this.invoiceList = response.data;
    });

    this.kurirService.getKurir().subscribe(response=>{
        this.kurirList = response.data;
    });

    this.kendaraanService.getKendaraan().subscribe(response=>{
        this.kendaraanList = response.data;
    });
  }

  generateForm(){
    this.formData = this.fb.group({
      tglSuratJalan : [ null, [ Validators.required ] ],
      namaPenerima : [ null, [ Validators.required ] ],
      alamatPenerima : [ null, [ Validators.required ] ],
      noInvoice : [ null, [ Validators.required ] ],
      kodeKendaraan : [ null, [ Validators.required ] ],
      kodeKurir : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    console.log("show form");
    this.isShowForm = true;
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;

    this.suratJalanService.postSuratJalan(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
    });
  }

  deleteData(noSuratJalan){
    console.log("delete");
    this.suratJalanService.deleteSuratJalan({noSuratJalan: noSuratJalan}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    this.isShowForm = false;
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }

  printData(data:any){
    const doc = new jsPDF();

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Surat Jalan", 90, 50);

    doc.autoTable({
      head: [['No SuratJalan', 'Tgl SuratJalan', 'Nama Penerima', 'Alamat Penerima', 'No Invoice', 'Kode Kendaraan', 'Kode Kurir']],
      body: [[data.noSuratJalan,moment(data.tglSuratJalan).format("DD-MM-YYYY"), data.namaPenerima, data.alamatPenerima,
        data.invoice.noInvoice, data.kendaraan.kodeKendaraan, data.kurir.kodeKurir ]],
      margin: {top: 70}
    });

    doc.save('Surat Jalan.pdf');
  }


}
