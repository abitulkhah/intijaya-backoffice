import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../providers/menu/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formData: FormGroup;

  constructor(private fb: FormBuilder, private router:Router, private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.formData = this.fb.group({
      username: [ null, [ Validators.required ] ],
      password: [ null, [ Validators.required ] ],
    });
  }


  submitForm() {
    let data = this.formData.value;
    this.loginService.auth(data).subscribe(response=>{
      if(response.access_token){
        localStorage.setItem("token", response.access_token);
        localStorage.setItem("username", data.username);
        this.router.navigate(["menu"]);
      }
    });
  }

  resetForm(){

    for (const i in this.formData.controls) {
      this.formData.controls[i].markAsDirty();
      this.formData.controls[i].updateValueAndValidity();
    }
  }

}
