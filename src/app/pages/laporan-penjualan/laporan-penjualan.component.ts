import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { LaporanPenjualanService } from '../../providers/menu/laporan-penjualan.service';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-laporan-penjualan',
  templateUrl: './laporan-penjualan.component.html',
  styleUrls: ['./laporan-penjualan.component.scss']     
})
export class LaporanPenjualanComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;
  grandTotal = 0;
  startDate  = null;
  endDate = null;
  showTable = false;
  
  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private laporanPenjualanService: LaporanPenjualanService,
    private imageEncoder : ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    // this.laporanPenjualanService.getLaporanPenjualan().subscribe(response=>{
    //   if(this.handlerResponseService.successResponse(response)){
    //     this.tableData = response.data;
    //     this.startDate = moment(this.tableData[0].tglPemesanan).format("DD-MM-YYYY");
    //     this.endDate = moment(this.tableData[this.tableData.length-1].tglPemesanan).format("DD-MM-YYYY");
    //     for (let index = 0; index < this.tableData.length; index++) {
    //       this.grandTotal = this.grandTotal + this.tableData[index].grandTotal;          
    //     }
    //   }
    // });
  }

  generateForm(){
    this.formData = this.fb.group({
      startDate: [ null, [ Validators.required ] ],
      endDate: [ null, [ Validators.required ] ],
    });
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    data.startDate = moment(data.startDate).format("YYYY-MM-DD 00:00:00");
    data.endDate = moment(data.endDate).format("YYYY-MM-DD 23:59:59");
    this.laporanPenjualanService.getLaporanPenjualan(data).subscribe(response=>{
      this.loadingSubmit = false;
      if(this.handlerResponseService.successResponse(response)){
        this.resetForm(e);
        this.tableData = response.data;
        for (let index = 0; index < this.tableData.length; index++) {
          this.grandTotal = this.grandTotal + this.tableData[index].grandTotal;          
        }
        this.startDate = moment(data.startDate).format("DD-MM-YYYY");
        this.endDate = moment(data.endDate).format("DD-MM-YYYY");
        this.showTable = true;
      }
    });

  }

  printData(){
    const doc = new jsPDF();
    let listData = [];
    let grandTotal = 0;
    for (let i = 0; i < this.tableData.length; i++) {
      grandTotal = grandTotal + this.tableData[i].grandTotal;
      let object = this.tableData[i];
      let data = {
        noCustomer: object.customer.noCustomer,
        namaCustomer: object.customer.namaCustomer,
        noPemesanan: object.noPemesanan,
        dp: object.dp,
        sisaBayar: object.sisaBayar,
        tglPemesanan: moment(object.tglPemesanan).format("DD-MM-YYYY"),
        grandTotal: object.grandTotal
      };
      listData.push(Object.values(data));
    }

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Laporan Penjualan", 80, 50);
    doc.setFontSize(10);
    doc.text("Periode :", 15, 80);
    doc.text(this.startDate + " s/d "+ this.endDate, 40, 80);
    doc.text("Grand Total :", 15, 85);
    doc.text("Rp. "+grandTotal, 45, 85);
  
    doc.autoTable({
      head: [['Tgl Pemesanan','No. Customer', 'Nama Customer', 'No. Pemesanan','DP','Sisa Bayar','Grand Total']],
      body: listData,
      margin: {top: 95}
    });

    doc.save('Laporan Penjualan.pdf');
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


}
