import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { ModelBarangService } from '../../providers/menu/model-barang.service';

@Component({
  selector: 'app-model-barang',
  templateUrl: './model-barang.component.html',
  styleUrls: ['./model-barang.component.scss']
})
export class ModelBarangComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  kodeData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private modelBarangService: ModelBarangService
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.modelBarangService.getModelBarang().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
    this.isShowForm = false;
  }

  generateForm(){
    this.formData = this.fb.group({
      type : [ null, [ Validators.required ] ]
    });
  }

  showForm(data){
    this.isShowForm = true;
    if(data){
      this.kodeData = data.kodeModelBarang;
      this.formData.patchValue(data);
    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;

    //edit data
    if(this.kodeData){
      data.kodeModelBarang = this.kodeData;
      this.modelBarangService.updateModelBarang(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
    //add data 
    else{
      console.log(JSON.stringify(data));
      this.modelBarangService.postModelBarang(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
  }

  deleteData(kodeModelBarang){
    console.log("delete");
    this.modelBarangService.deleteModelBarang({kodeModelBarang: kodeModelBarang}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    this.isShowForm = false;
    if(this.kodeData){
      this.kodeData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }

}
