import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelBarangComponent } from './model-barang.component';

describe('ModelBarangComponent', () => {
  let component: ModelBarangComponent;
  let fixture: ComponentFixture<ModelBarangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelBarangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelBarangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
