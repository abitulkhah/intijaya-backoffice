import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPemesananComponent } from './list-pemesanan.component';

describe('ListPemesananComponent', () => {
  let component: ListPemesananComponent;
  let fixture: ComponentFixture<ListPemesananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPemesananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPemesananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
