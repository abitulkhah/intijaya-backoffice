import { Component, OnInit } from '@angular/core';
import { PemesananBarangService } from '../../providers/menu/pemesanan-barang.service';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-list-pemesanan',
  templateUrl: './list-pemesanan.component.html',
  styleUrls: ['./list-pemesanan.component.scss']
})
export class ListPemesananComponent implements OnInit {

  tableData = [];
  tableBarang = [];
  constructor(
    private handlerResponseService: HandlerResponseService, 
    private pemesananBarangService: PemesananBarangService,
  ){}

  ngOnInit(): void {
    this.pemesananBarangService.getpemesananBarang().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
  }

}
