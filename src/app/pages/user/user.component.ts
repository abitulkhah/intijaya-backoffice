import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { UserService } from '../../providers/menu/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private userService: UserService
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.userService.getUsers().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
  }

  generateForm(){
    this.formData = this.fb.group({
      userName : [ null, [ Validators.required ] ],
      fullName : [ null, [ Validators.required ] ],
      email : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    console.log("show form");
    this.isShowForm = true;
    if(data){

    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    delete data.imageKadoStory;

    //edit data
    if(this.idData){
      if(data.image.split(",")[1]){
        delete data.image;
        delete data.fileName;
        delete data.mimeType;
      }
      data.id = this.idData
      this.userService.updateUsers(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
    //add data 
    else{
      console.log(JSON.stringify(data));
      this.userService.postUsers(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
  }

  deleteData(id){
    console.log("delete");
    this.userService.deleteUsers({id: id}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    if(this.idData){
      this.idData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


}
