import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { InvoiceService } from '../../providers/menu/invoice.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private invoiceService: InvoiceService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.invoiceService.getDetailInvoiceList().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
  }

  generateForm(){
    this.formData = this.fb.group({
      tglInvoice : [ null, [ Validators.required ] ],
      jmlTagihan : [ null, [ Validators.required ] ],
      noPesan : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    console.log("show form");
    this.isShowForm = true;
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;

    this.invoiceService.postInvoice(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
        }
    });
  }


  printData(data:any){
    const doc = new jsPDF();

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Invoice", 90, 50);

    doc.setFontSize(10);
    doc.text("No. Customer", 15, 55);
    doc.text(data.invoice.pemesanan.customer.noCustomer, 50, 55);
    doc.text("Nama Customer", 15, 60);
    doc.text(data.invoice.pemesanan.customer.namaCustomer, 50, 60);
    doc.text("No. Invoice", 15, 65);
    doc.text(data.invoice.noInvoice, 50, 65);
    doc.text("DP", 15, 70);
    doc.text(data.invoice.pemesanan.dp.toString(), 50, 70);
    doc.text("Grand Total", 15, 75);
    doc.text(data.invoice.pemesanan.grandTotal.toString(), 50, 75);
    doc.text("Sisa Bayar", 15, 80);
    doc.text(data.invoice.pemesanan.sisaBayar.toString(), 50, 80);
    doc.text("Tgl Invoice", 15, 85);
    doc.text(moment(data.invoice.tglInvoice).format("DD-MM-YYYY"), 50, 85);

    let listData = [];
    for (let i = 0; i < data.detailInvoiceList.length; i++) {
        let objecData = {
          kodeBarang: data.detailInvoiceList[i].kodeBarang,
          namaBarang: data.detailInvoiceList[i].namaBarang,
          hargaBarang: data.detailInvoiceList[i].hargaBarang,
          quantity: data.detailInvoiceList[i].quantity
        };
        listData.push(Object.values(objecData));
    }
    doc.autoTable({
      head: [['Kode Barang', 'Nama Barang', 'Harga', 'Quantity']],
      body: listData,
      margin: {top: 90}
    });

    doc.save('Invoice.pdf');
  }
}
