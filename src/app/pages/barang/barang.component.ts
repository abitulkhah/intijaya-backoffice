import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { BarangService } from '../../providers/menu/barang.service';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { ModelBarangService } from '../../providers/menu/model-barang.service';

@Component({
  selector: 'app-barang',
  templateUrl: './barang.component.html',
  styleUrls: ['./barang.component.scss']
})
export class BarangComponent implements OnInit {
  
  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  kodeData = null;
  loadingSubmit = false;
  listModelType = [];

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private barangService: BarangService,
    private modelBarangService: ModelBarangService
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.barangService.getBarang().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
    this.modelBarangService.getModelBarang().subscribe(response=>{
      this.listModelType = response.data;
    });
    this.isShowForm = false;
  }

  generateForm(){
    this.formData = this.fb.group({
      namaBarang : [ null, [ Validators.required ] ],
      satuan : [ null, [ Validators.required ] ],
      harga : [ null, [ Validators.required ] ],
      stok : [ null, [ Validators.required ] ],
      kodeModelBarang : [ null, [ Validators.required ] ], 
    });
  }

  showForm(data){
    console.log("show form");
    this.isShowForm = true;
    if(data){
      this.kodeData = data.kodeBarang;
      this.formData.patchValue(data);
      let kodelModelBarang = data.modelBarang.kodeModelBarang;
      this.formData.patchValue({kodeModelBarang: data.modelBarang.kodeModelBarang});
    }else{

    }
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;

    //edit data
    if(this.kodeData){
      data.kodeBarang = this.kodeData;
      this.barangService.updateBarang(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
    //add data 
    else{
      this.barangService.postBarang(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
      });
      
    }
  }

  deleteData(kodeBarang){
    console.log("delete");
    this.barangService.deleteBarang({kodeBarang: kodeBarang}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    if(this.kodeData){
      this.kodeData = null;
    }
    this.isShowForm = false;
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }

}
