import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPengirimanBarangComponent } from './laporan-pengiriman-barang.component';

describe('LaporanPengirimanBarangComponent', () => {
  let component: LaporanPengirimanBarangComponent;
  let fixture: ComponentFixture<LaporanPengirimanBarangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPengirimanBarangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPengirimanBarangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
