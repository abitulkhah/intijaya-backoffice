import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { LaporanPengirimanBarangService } from '../../providers/menu/laporan-pengiriman-barang.service';
import { SuratJalanService } from '../../providers/menu/surat-jalan.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-laporan-pengiriman-barang',
  templateUrl: './laporan-pengiriman-barang.component.html',
  styleUrls: ['./laporan-pengiriman-barang.component.scss']
})
export class LaporanPengirimanBarangComponent implements OnInit {

  tableData = [];
  formData: FormGroup;
  startDate = null;
  endDate = null;
  showTable = false;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private laporanPengirimanBarangService: LaporanPengirimanBarangService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    // this.suratJalanService.getSuratJalan().subscribe(response=>{
    //     this.tableData = response.data;
    //     this.startDate = moment(this.tableData[0].tglSuratJalan).format("DD-MM-YYYY");
    //     this.endDate = moment(this.tableData[this.tableData.length-1].tglSuratJalan).format("DD-MM-YYYY");
    // });
  }

  generateForm(){
    this.formData = this.fb.group({
      startDate: [ null, [ Validators.required ] ],
      endDate: [ null, [ Validators.required ] ],
   });
  }

  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    data.startDate = moment(data.startDate).format("YYYY-MM-DD 00:00:00");
    data.endDate = moment(data.endDate).format("YYYY-MM-DD 23:59:59");
    this.laporanPengirimanBarangService.getLaporanPengirimanBarang(data).subscribe(response=>{
      this.loadingSubmit = false;
      if(this.handlerResponseService.successResponse(response)){
        this.resetForm(e);
        this.tableData = response.data;
        this.startDate = moment(data.startDate).format("DD-MM-YYYY");
        this.endDate = moment(data.endDate).format("DD-MM-YYYY");
        this.showTable = true;
      }
    });

  }

  printData(){
    const doc = new jsPDF();

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Laporan Pengiriman Barang", 80, 50);
    doc.setFontSize(10);
    doc.text("Periode :", 15, 80);
    doc.text(this.startDate + " s/d "+ this.endDate, 40, 80);

    let listData = [];
    for (let i = 0; i < this.tableData.length; i++) {
      let object = this.tableData[i];
      let data = {
        noSuratJalan: object.noSuratJalan,
        namaPenerima: object.namaPenerima,
        alamatPenerima: object.alamatPenerima,
        noInvoice: object.invoice.noInvoice,
        kodeKendaraan: object.kendaraan.kodeKendaraan,
        kodeKurir: object.kurir.kodeKurir,
        tglSuratJalan: moment(object.tglSuratJalan).format("DD-MM-YYYY"),

      };
      listData.push(Object.values(data));
    }
    doc.autoTable({
      head: [[
        'Nomor Surat Jalan', 'Nama Penerima', 'Alamat Penerima','No. Invoice', 'Kode Kendaraan', 'Kode Kurir', 'Tgl Surat Jalan']
      ],
      body: listData,
      margin: {top: 85}
    });

    doc.save('Laporan Pengiriman Barang.pdf');
  }

  resetForm(e): void {
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }

}
