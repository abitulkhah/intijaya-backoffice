import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPembayaranComponent } from './laporan-pembayaran.component';

describe('LaporanPembayaranComponent', () => {
  let component: LaporanPembayaranComponent;
  let fixture: ComponentFixture<LaporanPembayaranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPembayaranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPembayaranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
