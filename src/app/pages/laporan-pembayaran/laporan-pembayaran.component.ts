import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { LaporanPembayaranService } from '../../providers/menu/laporan-pembayaran.service';
import { KwitansiService } from '../../providers/menu/kwitansi.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-laporan-pembayaran',
  templateUrl: './laporan-pembayaran.component.html',
  styleUrls: ['./laporan-pembayaran.component.scss']
})
export class LaporanPembayaranComponent implements OnInit {

  tableData = [];
  formData: FormGroup;
  startDate = null;
  endDate = null;
  showPeriode = false;
  isPrint = false;
  showTable = false;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private laporanPembayaranService: LaporanPembayaranService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    // this.kwitansiService.getKwitansi().subscribe(response=>{
    //     this.tableData = response.data;
    //     this.startDate = moment(this.tableData[0].tglKwitansi).format("DD-MM-YYYY");
    //     this.endDate = moment(this.tableData[this.tableData.length-1].tglKwitansi).format("DD-MM-YYYY");
    //     this.showPeriode = true;
    // });
  }

  generateForm(){
    this.formData = this.fb.group({
      startDate: [ null, [ Validators.required ] ],
      endDate: [ null, [ Validators.required ] ],
    });
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    data.startDate = moment(data.startDate).format("YYYY-MM-DD 00:00:00");
    data.endDate = moment(data.endDate).format("YYYY-MM-DD 23:59:59");
    this.laporanPembayaranService.getLaporanPembayaran(data).subscribe(response=>{
      this.loadingSubmit = false;
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
        this.startDate = moment(data.startDate).format("DD-MM-YYYY");
        this.endDate = moment(data.endDate).format("DD-MM-YYYY");
        this.showTable = true;
      }
    });

  }

  printData(){
    
    const doc = new jsPDF();
    
    let listData = [];

    let grandTotal = 0;
    for (let i = 0; i < this.tableData.length; i++) {
      grandTotal = grandTotal + this.tableData[i].jmlBayar;
      let object = this.tableData[i];
      let data = {
        tglKwitansi: moment(object.tglKwitansi).format("DD-MM-YYYY"),
        noKwitansi: object.noKwitansi,
        noInvoice: object.invoice.noInvoice,
        jmlBayar: object.jmlBayar
      };
      listData.push(Object.values(data));
    }

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Laporan Pembayaran", 80, 50);
    doc.setFontSize(10);
    doc.text("Periode :", 15, 80);
    doc.text(this.startDate + " s/d "+ this.endDate, 40, 80);
    doc.text("Grand Total :", 15, 85);
    doc.text("Rp. "+grandTotal, 45, 85);

    doc.autoTable({
      head: [['Nomor Surat Jalan', 'Nama Penerima', 'No Invoice', 'Jml Bayar']],
      body: listData,
      margin: {top: 95}
    });

    doc.save('Laporan Pembayaran.pdf');
  }
}
