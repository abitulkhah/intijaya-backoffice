import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { LaporanReturBarangService } from '../../providers/menu/laporan-retur-barang.service';
import { ReturBarangService } from '../../providers/menu/retur-barang.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-laporan-retur-barang',
  templateUrl: './laporan-retur-barang.component.html',
  styleUrls: ['./laporan-retur-barang.component.scss']
})
export class LaporanReturBarangComponent implements OnInit {

  tableData = [];
  formData: FormGroup;
  startDate = null;
  endDate = null;
  showTable = false;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private laporanReturBarangService: LaporanReturBarangService,
    private imageEncoder : ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    // this.returBarangService.getReturBarang().subscribe(response=>{
    //     this.tableData = response.data;
    //     console.log(this.tableData);
    //     this.startDate = moment(this.tableData[0].tglRetur).format("DD-MM-YYYY");
    //     this.endDate = moment(this.tableData[this.tableData.length-1].tglRetur).format("DD-MM-YYYY");
    // });
  }

  generateForm(){
    this.formData = this.fb.group({
      startDate: [ null, [ Validators.required ] ],
      endDate: [ null, [ Validators.required ] ],
    });
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;
    data.startDate = moment(data.startDate).format("YYYY-MM-DD 00:00:00");
    data.endDate = moment(data.endDate).format("YYYY-MM-DD 23:59:59");
    this.laporanReturBarangService.getLaporanReturBarang(data).subscribe(response=>{
      this.loadingSubmit = false;
      if(this.handlerResponseService.successResponse(response)){
        this.resetForm(e);
        this.tableData = response.data;
        this.startDate = moment(data.startDate).format("DD-MM-YYYY");
        this.endDate = moment(data.endDate).format("DD-MM-YYYY");
        this.showTable = true;
      }
    });

  }

  printData(){
    const doc = new jsPDF();
    let listData = [];

    for (let i = 0; i < this.tableData.length; i++) {
      let object = this.tableData[i];
      let data = {
        tglRetur: moment(object.tglRetur).format("DD-MM-YYYY"),
        noSuratJalan: object.noSuratJalan,
        noReceipt: object.receipt.noReceipt,
        jmlRetur: object.jmlRetur,
        ketRetur: object.ketRetur
      };
      listData.push(Object.values(data));
    }

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Laporan Retur Barang", 80, 50);
    doc.setFontSize(10);
    doc.text("Periode :", 15, 80);
    doc.text(this.startDate + " s/d "+ this.endDate, 40, 80);
  
    doc.autoTable({
      head: [['Tgl Retur','No Receipt','No Surat Jalan', 'Jumlah Retur', 'Keterangan Retur']],
      body: listData,
      margin: {top: 85}
    });

    doc.save('Laporan Retur Barang.pdf');
  }


  resetForm(e): void {
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }

}
