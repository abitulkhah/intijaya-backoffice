import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanReturBarangComponent } from './laporan-retur-barang.component';

describe('LaporanReturBarangComponent', () => {
  let component: LaporanReturBarangComponent;
  let fixture: ComponentFixture<LaporanReturBarangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanReturBarangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanReturBarangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
