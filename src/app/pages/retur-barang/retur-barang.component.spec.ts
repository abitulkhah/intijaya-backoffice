import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturBarangComponent } from './retur-barang.component';

describe('ReturBarangComponent', () => {
  let component: ReturBarangComponent;
  let fixture: ComponentFixture<ReturBarangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturBarangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturBarangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
