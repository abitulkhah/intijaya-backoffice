import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import { ReturBarangService } from '../../providers/menu/retur-barang.service';
import { BuktiTerimaService } from '../../providers/menu/bukti-terima.service';
import { BarangService } from '../../providers/menu/barang.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-retur-barang',
  templateUrl: './retur-barang.component.html',
  styleUrls: ['./retur-barang.component.scss']
})
export class ReturBarangComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;
  listBarang = [];
  listBuktiTerima = [];

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private returBarangService: ReturBarangService,
    private buktiTerimaService: BuktiTerimaService,
    private barangService: BarangService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.returBarangService.getReturBarang().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
    this.buktiTerimaService.getBuktiTerima().subscribe(response=>{
        this.listBuktiTerima = response.data;
    });

    this.barangService.getBarang().subscribe(response=>{
      this.listBarang = response.data;
    });
  }

  generateForm(){
    this.formData = this.fb.group({
      noReceipt :  [ null, [ Validators.required ]],
      kodeBarang : [ null, [ Validators.required ]],
      jmlRetur :  [ null, [ Validators.required ]],
      ketRetur : [ null, [ Validators.required ]],
      tglRetur : [ null, [ Validators.required ]]
    });
  }

  showForm(data){
    this.isShowForm = true;
  }


  submitForm(e){

    this.loadingSubmit = true;
    let data = this.formData.value;

    this.returBarangService.postReturBarang(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
    });
    
  }

  deleteData(id){
    console.log("delete");
    this.returBarangService.deleteReturBarang({id: id}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    this.isShowForm = false;
    if(this.idData){
      this.idData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }

  printData(data:any){
    const doc = new jsPDF();

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Retur Barang", 90, 50);

    doc.autoTable({
      head: [['Tgl Retur', 'Tgl Terima Barang', 'No Surat Jalan', 'Jumlah Retur', 'Keterangan Retur']],
      body: [[moment(data.tglRetur).format("DD-MM-YYYY"), moment(data.receipt.noReceipt).format("DD-MM-YYYY"), data.receipt.suratJalan.noSuratJalan, data.jmlRetur, data.ketRetur]],
      margin: {top: 70}
    });

    doc.save('Retur Barang.pdf');
  }


}
