import { Component, OnInit } from '@angular/core';
import { ModelBarangService } from '../../providers/menu/model-barang.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  username = localStorage.getItem("username");
  
  constructor(private modelBarangService: ModelBarangService) { }

  ngOnInit() {
  
  }

}
