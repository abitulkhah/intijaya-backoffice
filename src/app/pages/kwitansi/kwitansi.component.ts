import { Component, OnInit } from '@angular/core';
import { KwitansiService } from '../../providers/menu/kwitansi.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HandlerResponseService } from '../../providers/handler-response/handler-response.service';
import * as moment from 'moment';
import { ImageEncoder } from '../../utils/image-encoder';
declare let jsPDF:any;

@Component({
  selector: 'app-kwitansi',
  templateUrl: './kwitansi.component.html',
  styleUrls: ['./kwitansi.component.scss']
})
export class KwitansiComponent implements OnInit {

  isShowForm = false;
  tableData = [];
  formData: FormGroup;
  idData = null;
  loadingSubmit = false;

  constructor(
    private fb: FormBuilder, 
    private handlerResponseService: HandlerResponseService, 
    private kwitansiService: KwitansiService,
    private imageEncoder: ImageEncoder
  ){}

  ngOnInit(): void {
    this.generateForm();
    this.kwitansiService.getKwitansi().subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
        this.tableData = response.data;
      }
    });
  }

  generateForm(){
    this.formData = this.fb.group({
      tglKwitansi : [ null, [ Validators.required ] ],
      jmlBayar : [ null, [ Validators.required ] ],
      noInvoice : [ null, [ Validators.required ] ],
    });
  }

  showForm(data){
    console.log("show form");
    this.isShowForm = true;
    if(data){

    }else{

    }
  }


  submitForm(e){
    this.loadingSubmit = true;
    let data = this.formData.value;
    this.kwitansiService.postKwitansi(data).subscribe(response=>{
        this.loadingSubmit = false;
        if(this.handlerResponseService.successResponse(response)){
          this.resetForm(e);
          this.ngOnInit();
        }
    });
  }

  deleteData(id){
    console.log("delete");
    this.kwitansiService.deleteKwitansi({id: id}).subscribe(response=>{
      if(this.handlerResponseService.successResponse(response)){
          this.ngOnInit();
      }
    });
  }

  cancelDelete(){
    console.log("cancel delete");
  }

  resetForm(e): void {
    if(this.idData){
      this.idData = null;
    }
    e.preventDefault();
    this.formData.reset();
    for (const key in this.formData.controls) {
      this.formData.controls[key].markAsPristine();
      this.formData.controls[key].updateValueAndValidity();
    }
  }


  printData(data:any){
    const doc = new jsPDF();

    doc.addImage(this.imageEncoder.imageLogoBase64, 'JPEG', 10, 5, 80, 30);
    doc.text("Kwitansi", 90, 50);

    doc.autoTable({
      head: [['Tgl Kwitansi','No Customer','Nama Customer', 'No Kwitansi', 'No Invoice','Jml Bayar']],
      body: [[moment(data.tglKwitansi).format("DD-MM-YYYY"), data.invoice.pemesanan.customer.noCustomer, data.invoice.pemesanan.customer.namaCustomer, data.noKwitansi, data.invoice.noInvoice, data.jmlBayar]],
      margin: {top: 70}
    });

    doc.save('Kwitansi.pdf');
  }


}
