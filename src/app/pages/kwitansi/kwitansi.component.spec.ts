import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KwitansiComponent } from './kwitansi.component';

describe('KwitansiComponent', () => {
  let component: KwitansiComponent;
  let fixture: ComponentFixture<KwitansiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KwitansiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KwitansiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
