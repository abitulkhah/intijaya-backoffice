import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  menuData = [
      {
        parentMenu : "Dashboard",
        icon: "home",
        routerLink: "home"
      },
      {
        parentMenu : "Data Master",
        icon: "table",
        childMenu: [
          {
            name: "Customer",
            routerLink: "customer"
          },
          {
            name: "Model Barang",
            routerLink: "model-barang"
          },
          {
            name: "Barang",
            routerLink: "barang"
          },
          {
            name: "Kurir",
            routerLink: "kurir"
          },
          {
            name: "Kendaraan",
            routerLink: "kendaraan"
          },
          // {
          //   name: "User",
          //   routerLink: "user"
          // }
        ]
      },
      {
        parentMenu : "Data Transaksi",
        icon: "export",
        childMenu: [
          {
            name: "Pemesanan Barang",
            routerLink: "pemesanan-barang"
          },
          {
            name: "List Pemesanan",
            routerLink: "list-pemesanan"
          },
          {
            name: "Surat Jalan",
            routerLink: "surat-jalan"
          },
          {
            name: "Invoice",
            routerLink: "invoice"
          },
          {
            name: "Kwitansi",
            routerLink: "kwitansi"
          },
          {
            name: "Bukti Terima",
            routerLink: "bukti-terima"
          },
          {
            name: "Retur Barang",
            routerLink: "retur-barang"
          }
        ]
      },
      {
        parentMenu : "Data Laporan",
        icon: "file",
        childMenu: [
          {
            name: "Pembayaran",
            routerLink: "laporan-pembayaran"
          },
          {
            name: "Pengiriman Barang",
            routerLink: "laporan-pengiriman-barang"
          },
          {
            name: "Penjualan",
            routerLink: "laporan-penjualan"
          },
          {
            name: "Retur Barang",
            routerLink: "laporan-retur-barang"
          }
        ]
      },
      {
        parentMenu : "Logout",
        icon: "logout",
        routerLink: "/login"
      }
  ];

  username = localStorage.getItem("username");
  constructor() { }

  ngOnInit() {
  }

  isCollapsed = false;
  triggerTemplate = null;
  @ViewChild('trigger') customTrigger: TemplateRef<void>;

  /** custom trigger can be TemplateRef **/
  changeTrigger(): void {
    this.triggerTemplate = this.customTrigger;
  }

}
