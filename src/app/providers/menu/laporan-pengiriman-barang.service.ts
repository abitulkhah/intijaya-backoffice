import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class LaporanPengirimanBarangService {

  constructor(private commonApi: CommonApiService) { }

  getLaporanPengirimanBarang(params?){
    return this.commonApi.get("v1/suratJalan/reportPengirimanBarang", params);
  }

}
