import { TestBed } from '@angular/core/testing';

import { LaporanReturBarangService } from './laporan-retur-barang.service';

describe('LaporanReturBarangService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaporanReturBarangService = TestBed.get(LaporanReturBarangService);
    expect(service).toBeTruthy();
  });
});
