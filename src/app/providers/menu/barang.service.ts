import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class BarangService {

  constructor(private commonApi: CommonApiService) { }

  getBarang(){
    return this.commonApi.get("v1/barang");
  }

  postBarang(bodyRequest: any){
    return this.commonApi.post("v1/barang", bodyRequest);
  }

  updateBarang(bodyRequest: any){
    return this.commonApi.put("v1/barang", bodyRequest);
  }

  deleteBarang(bodyOptions: any){
    return this.commonApi.delete("v1/barang", bodyOptions);
  }
}
