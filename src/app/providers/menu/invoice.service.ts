import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private commonApi: CommonApiService) { }

  getInvoice(){
    return this.commonApi.get("v1/invoice");
  }

  getDetailInvoiceList(){
    return this.commonApi.get("v1/invoice/detailInvoiceList");
  }

  postInvoice(bodyRequest: any){
    return this.commonApi.post("v1/invoice", bodyRequest);
  }

  updateInvoice(bodyRequest: any){
    return this.commonApi.put("v1/invoice", bodyRequest);
  }

  deleteInvoice(bodyOptions: any){
    return this.commonApi.delete("v1/invoice", bodyOptions);
  }
}
