import { TestBed } from '@angular/core/testing';

import { PemesananBarangService } from './pemesanan-barang.service';

describe('PemesananBarangService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PemesananBarangService = TestBed.get(PemesananBarangService);
    expect(service).toBeTruthy();
  });
});
