import { TestBed } from '@angular/core/testing';

import { SuratJalanService } from './surat-jalan.service';

describe('SuratJalanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuratJalanService = TestBed.get(SuratJalanService);
    expect(service).toBeTruthy();
  });
});
