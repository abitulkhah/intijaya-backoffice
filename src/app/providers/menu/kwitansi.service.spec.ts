import { TestBed } from '@angular/core/testing';

import { KwitansiService } from './kwitansi.service';

describe('KwitansiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KwitansiService = TestBed.get(KwitansiService);
    expect(service).toBeTruthy();
  });
});
