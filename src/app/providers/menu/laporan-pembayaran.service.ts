import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class LaporanPembayaranService {

  constructor(private commonApi: CommonApiService) { }

  getLaporanPembayaran(params?){
    return this.commonApi.get("v1/kwitansi/reportPembayaran", params);
  }
}
