import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class LaporanPenjualanService {

  constructor(private commonApi: CommonApiService) { }

  getLaporanPenjualan(params?){
    return this.commonApi.get("v1/pemesanan/reportPenjualan", params);
  }
  
}
