import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class KendaraanService {

  constructor(private commonApi: CommonApiService) { }

  getKendaraan(){
    return this.commonApi.get("v1/kendaraan");
  }

  postKendaraan(bodyRequest: any){
    return this.commonApi.post("v1/kendaraan", bodyRequest);
  }

  updateKendaraan(bodyRequest: any){
    return this.commonApi.put("v1/kendaraan", bodyRequest);
  }

  deleteKendaraan(bodyOptions: any){
    return this.commonApi.delete("v1/kendaraan", bodyOptions);
  }
}
