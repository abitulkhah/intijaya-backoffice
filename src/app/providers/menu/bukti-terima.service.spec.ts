import { TestBed } from '@angular/core/testing';

import { BuktiTerimaService } from './bukti-terima.service';

describe('BuktiTerimaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BuktiTerimaService = TestBed.get(BuktiTerimaService);
    expect(service).toBeTruthy();
  });
});
