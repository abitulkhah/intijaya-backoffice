import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class KurirService {

  constructor(private commonApi: CommonApiService) { }

  getKurir(){
    return this.commonApi.get("v1/kurir");
  }

  postKurir(bodyRequest: any){
    return this.commonApi.post("v1/kurir", bodyRequest);
  }

  updateKurir(bodyRequest: any){
    return this.commonApi.put("v1/kurir", bodyRequest);
  }

  deleteKurir(bodyOptions: any){
    return this.commonApi.delete("v1/kurir", bodyOptions);
  }
}
