import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class ReturBarangService {

  constructor(private commonApi: CommonApiService) { }

  getReturBarang(){
    return this.commonApi.get("v1/detailReceipt");
  }

  postReturBarang(bodyRequest: any){
    return this.commonApi.post("v1/detailReceipt", bodyRequest);
  }

  updateReturBarang(bodyRequest: any){
    return this.commonApi.put("v1/detailReceipt", bodyRequest);
  }

  deleteReturBarang(bodyOptions: any){
    return this.commonApi.delete("v1/detailReceipt", bodyOptions);
  }
}
