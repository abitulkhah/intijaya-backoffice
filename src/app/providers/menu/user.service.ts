import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private commonApi: CommonApiService) { }

  getUsers(){
    return this.commonApi.get("v1/users");
  }

  postUsers(bodyRequest: any){
    return this.commonApi.post("v1/users", bodyRequest);
  }

  updateUsers(bodyRequest: any){
    return this.commonApi.put("v1/users", bodyRequest);
  }

  deleteUsers(bodyOptions: any){
    return this.commonApi.delete("v1/users", bodyOptions);
  }
}
