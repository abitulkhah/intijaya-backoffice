import { TestBed } from '@angular/core/testing';

import { LaporanPembayaranService } from './laporan-pembayaran.service';

describe('LaporanPembayaranService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaporanPembayaranService = TestBed.get(LaporanPembayaranService);
    expect(service).toBeTruthy();
  });
});
