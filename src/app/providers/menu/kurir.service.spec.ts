import { TestBed } from '@angular/core/testing';

import { KurirService } from './kurir.service';

describe('KurirService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KurirService = TestBed.get(KurirService);
    expect(service).toBeTruthy();
  });
});
