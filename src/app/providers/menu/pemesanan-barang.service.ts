import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class PemesananBarangService {

  constructor(private commonApi: CommonApiService) { }

  getpemesananBarang(){
    return this.commonApi.get("v1/pemesanan");
  }

  postpemesananBarang(bodyRequest: any){
    return this.commonApi.post("v1/pemesanan", bodyRequest);
  }

  updatepemesananBarang(bodyRequest: any){
    return this.commonApi.put("v1/pemesanan", bodyRequest);
  }

  deletepemesananBarang(bodyOptions: any){
    return this.commonApi.delete("v1/pemesanan", bodyOptions);
  }
}
