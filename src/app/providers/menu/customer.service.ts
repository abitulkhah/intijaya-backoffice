import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private commonApi: CommonApiService) { }

  getCustomer(){
    return this.commonApi.get("v1/customer");
  }

  postCustomer(bodyRequest: any){
    return this.commonApi.post("v1/customer", bodyRequest);
  }

  updateCustomer(bodyRequest: any){
    return this.commonApi.put("v1/customer", bodyRequest);
  }

  deleteCustomer(bodyOptions: any){
    return this.commonApi.delete("v1/customer", bodyOptions);
  }
}
