import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class SuratJalanService {

  constructor(private commonApi: CommonApiService) { }

  getSuratJalan(){
    return this.commonApi.get("v1/suratJalan");
  }

  postSuratJalan(bodyRequest: any){
    return this.commonApi.post("v1/suratJalan", bodyRequest);
  }

  updateSuratJalan(bodyRequest: any){
    return this.commonApi.put("v1/suratJalan", bodyRequest);
  }

  deleteSuratJalan(bodyOptions: any){
    return this.commonApi.delete("v1/suratJalan", bodyOptions);
  }
}
