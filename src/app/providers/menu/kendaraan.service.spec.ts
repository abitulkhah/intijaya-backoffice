import { TestBed } from '@angular/core/testing';

import { KendaraanService } from './kendaraan.service';

describe('KendaraanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KendaraanService = TestBed.get(KendaraanService);
    expect(service).toBeTruthy();
  });
});
