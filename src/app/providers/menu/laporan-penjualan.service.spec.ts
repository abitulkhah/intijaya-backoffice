import { TestBed } from '@angular/core/testing';

import { LaporanPenjualanService } from './laporan-penjualan.service';

describe('LaporanPenjualanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaporanPenjualanService = TestBed.get(LaporanPenjualanService);
    expect(service).toBeTruthy();
  });
});
