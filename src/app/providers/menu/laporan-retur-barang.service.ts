import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class LaporanReturBarangService {

  constructor(private commonApi: CommonApiService) { }

  getLaporanReturBarang(params?){
    return this.commonApi.get("v1/detailReceipt/reportReturBarang", params);
  }
}
