import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class BuktiTerimaService {

  constructor(private commonApi: CommonApiService) { }

  getBuktiTerima(){
    return this.commonApi.get("v1/receipt");
  }

  postBuktiTerima(bodyRequest: any){
    return this.commonApi.post("v1/receipt", bodyRequest);
  }

  updateBuktiTerima(bodyRequest: any){
    return this.commonApi.put("v1/receipt", bodyRequest);
  }

  deleteBuktiTerima(bodyOptions: any){
    return this.commonApi.delete("v1/receipt", bodyOptions);
  }}
