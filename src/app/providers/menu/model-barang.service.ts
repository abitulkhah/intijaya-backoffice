import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class ModelBarangService {

  listModelBarang = [];
  constructor(private commonApi: CommonApiService) { }

  getModelBarang(){
    return this.commonApi.get("v1/modelBarang");
  }

  postModelBarang(bodyRequest: any){
    return this.commonApi.post("v1/modelBarang", bodyRequest);
  }

  updateModelBarang(bodyRequest: any){
    return this.commonApi.put("v1/modelBarang", bodyRequest);
  }

  deleteModelBarang(bodyOptions: any){
    return this.commonApi.delete("v1/modelBarang", bodyOptions);
  }
}
