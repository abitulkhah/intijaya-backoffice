import { TestBed } from '@angular/core/testing';

import { ModelBarangService } from './model-barang.service';

describe('ModelBarangService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModelBarangService = TestBed.get(ModelBarangService);
    expect(service).toBeTruthy();
  });
});
