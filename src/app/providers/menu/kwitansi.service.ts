import { Injectable } from '@angular/core';
import { CommonApiService } from '../common-api/common-api.service';

@Injectable({
  providedIn: 'root'
})
export class KwitansiService {

  constructor(private commonApi: CommonApiService) { }

  getKwitansi(){
    return this.commonApi.get("v1/kwitansi");
  }

  postKwitansi(bodyRequest: any){
    return this.commonApi.post("v1/kwitansi", bodyRequest);
  }

  updateKwitansi(bodyRequest: any){
    return this.commonApi.put("v1/kwitansi", bodyRequest);
  }

  deleteKwitansi(bodyOptions: any){
    return this.commonApi.delete("v1/kwitansi", bodyOptions);
  }
}
