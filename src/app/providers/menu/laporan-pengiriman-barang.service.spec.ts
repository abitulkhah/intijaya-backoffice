import { TestBed } from '@angular/core/testing';

import { LaporanPengirimanBarangService } from './laporan-pengiriman-barang.service';

describe('LaporanPengirimanBarangService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaporanPengirimanBarangService = TestBed.get(LaporanPengirimanBarangService);
    expect(service).toBeTruthy();
  });
});
