import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEvent } from '@angular/common/http';
import { Config } from '../../config/config';
import { HandlerResponseService } from '../handler-response/handler-response.service';
import { NzMessageService } from 'ng-zorro-antd';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonApiService {

  BASE_URL : string;
  headers = null;

  constructor(private http: HttpClient, private config: Config, private handlerResponseService: HandlerResponseService, private message: NzMessageService) 
  {
    this.BASE_URL = config.BASE_URL;
  }

  getHeaders() {

    this.headers = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem("token"));
    return this.headers;

  }


  get(url: string, params?: any, reqOpts?: any) : Observable<any> {
      
      if (!reqOpts) {
        reqOpts = {
          params: new HttpParams()
        };
      }
      
      if (params) {
  
        reqOpts.params = new HttpParams();
        
        for (let reqParams in params) {
          reqOpts.params = reqOpts.params.set(reqParams, params[reqParams]);
        }
  
      }
      if (localStorage.getItem("token")) {
        reqOpts.headers = this.getHeaders();
      }
  
      return this.http.get<any>(this.BASE_URL + url, reqOpts)
      .pipe(catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              this.handlerResponseService.failedResponse(error);
              throw error;
            }
        ),
      );

  }

  post(url: string, body: any, reqOpts?: any) : Observable<any> {
    
      if (localStorage.getItem("token")) {
        reqOpts = {
          headers : this.getHeaders()
        };
      }
  
      return this.http.post<any>(this.BASE_URL + url, body, reqOpts)
      .pipe(
        catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              this.handlerResponseService.failedResponse(error);
              throw error;
            }
        ),
      );
    
  }

  put(url: string, body: any, reqOpts?: any) : Observable<any> {
      
        if (!reqOpts) {
          reqOpts = {
            params: new HttpParams()
          };
        }
        
        if (localStorage.getItem("token")) {
          reqOpts.headers = this.getHeaders();
        }

        return this.http.put<any>(this.BASE_URL+ url, body, reqOpts)
        .pipe(
          catchError(
              (error: any, caught: Observable<HttpEvent<any>>) => {
                this.handlerResponseService.failedResponse(error);
                throw error;
              }
          ),
        );

  }

  delete(url: string, body?: any) : Observable<any> {

      let reqOpts : any;

      if (localStorage.getItem("token")) {
        reqOpts = {
          body: body,
          headers : this.getHeaders()
        }
      }
  
      console.log(reqOpts);

      return this.http.delete<any>(this.BASE_URL + url, reqOpts)
      .pipe(
        catchError(
            (error: any, caught: Observable<HttpEvent<any>>) => {
              this.handlerResponseService.failedResponse(error);
              throw error;
            }
        ),
      );

  }

  validate(url: string, body: any, reqOpts?: any) :  Observable<any> {

        const authHeaders = new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded"})
                              .set("Content-Type", "application/x-www-form-urlencoded")
                              .set("Authorization", "Basic "+btoa("intijaya-client:password123"));
        reqOpts = { headers : authHeaders};
        let bodyUrl = new URLSearchParams();
        bodyUrl.set("username", body.username);
        bodyUrl.set("password", body.password);
        bodyUrl.set("grant_type", "password");    
        return this.http.post<any>(this.BASE_URL + url, bodyUrl.toString(),reqOpts)
        .pipe(
          catchError(
              (error: any, caught: Observable<HttpEvent<any>>) => {
                this.handlerResponseService.failedResponse(error);
                throw error;
              }
          ),
        );
    
  }

  checkConnection(){
    return navigator.onLine;
  }
}
