import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Config {
    public readonly DEV_ENV  = "http://localhost:8081/";
    public readonly PROD_ENV  = "http://localhost:8081/";
    public readonly BASE_URL = environment.production? this.PROD_ENV : this.DEV_ENV;
}
